package com.apod.nasa

import android.app.Application
import com.apod.nasa.di.component.ApplicationComponent
import com.apod.nasa.di.component.DaggerApplicationComponent
import com.apod.nasa.di.module.ApplicationModule

/**
 * Created by Pallab Banerjee on 3/8/2021.
 */
/**
 * [NasaApodApplication] class created by extending Application class
 * to help build the singleton dependencies via dagger.
 **/

class NasaApodApplication : Application() {


    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        injectDependencies()
    }

    private fun injectDependencies() {

      applicationComponent =   DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
      applicationComponent.inject(this)

    }

    // replace the component with a test specific one
    fun setApplicationComponentForTest(applicationComponent: ApplicationComponent) {
        this.applicationComponent = applicationComponent
    }

}