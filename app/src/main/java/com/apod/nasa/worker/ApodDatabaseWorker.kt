package com.apod.nasa.worker

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.apod.nasa.data.local.ApodDataItem
import com.apod.nasa.data.local.AppDatabase
import com.apod.nasa.utils.DATA_FILENAME
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import kotlinx.coroutines.coroutineScope
import javax.inject.Inject

/**
 * Created by Pallab Banerjee on 3/9/2021.
 */

class ApodDatabaseWorker (
   context : Context,
   workerParams: WorkerParameters
) : CoroutineWorker(context,workerParams){


   override suspend fun doWork(): Result = coroutineScope{
      try {
         applicationContext.assets.open(DATA_FILENAME).use { inputStream ->
            JsonReader(inputStream.reader()).use { jsonReader ->
               val apodType = object : TypeToken<List<ApodDataItem>>() {}.type
               val apodList : List<ApodDataItem> = Gson().fromJson(jsonReader,apodType)
               val database : AppDatabase = AppDatabase.getInstance(applicationContext)
               database.apodDao().insertAll(apodList)
               Result.success()
            }
         }
      } catch (ex: Exception) {
         Log.e(TAG, "Error seeding database", ex)
         Result.failure()
      }
   }
   companion object {
      private const val TAG = "ApodDatabaseWorker"
   }

}



