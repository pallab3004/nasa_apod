package com.apod.nasa.di.component

import com.apod.nasa.NasaApodApplication
import com.apod.nasa.data.local.ApodDao
import com.apod.nasa.data.local.AppDatabase
import com.apod.nasa.data.repository.ApodImageListRepository
import com.apod.nasa.di.module.ApplicationModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Pallab Banerjee on 3/9/2021.
 */

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(application: NasaApodApplication)

    fun getAppDatabase() : AppDatabase

    fun getApodImageListRepository() : ApodImageListRepository
}