package com.apod.nasa.di.module

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.apod.nasa.data.repository.ApodImageListRepository
import com.apod.nasa.databinding.ActivityMainBinding
import com.apod.nasa.di.ActivityScope
import com.apod.nasa.ui.main.ApodImageGridAdapter
import com.apod.nasa.ui.main.ApodImageViewModel
import com.apod.nasa.utils.ViewModelProviderFactory
import dagger.Module
import dagger.Provides

/**
 * Created by Pallab Banerjee on 3/10/2021.
 */

@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    @ActivityScope
    fun provideApodImageViewModel(
        apodImageListRepository: ApodImageListRepository
    ): ApodImageViewModel =
        ViewModelProvider(activity, ViewModelProviderFactory(ApodImageViewModel::class) {
            ApodImageViewModel(apodImageListRepository)
        }).get(ApodImageViewModel::class.java)


    @ActivityScope
    @Provides
    fun provideMainActivityViewBinding() : ActivityMainBinding = ActivityMainBinding.inflate(activity.layoutInflater)


    @ActivityScope
    @Provides
    fun provideApodImageGridAdapter() : ApodImageGridAdapter = ApodImageGridAdapter()

}