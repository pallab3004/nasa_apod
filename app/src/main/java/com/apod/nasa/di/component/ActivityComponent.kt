package com.apod.nasa.di.component

import com.apod.nasa.di.ActivityScope
import com.apod.nasa.di.module.ActivityModule
import com.apod.nasa.ui.main.MainActivity
import dagger.Component

/**
 * Created by Pallab Banerjee on 3/10/2021.
 */

@ActivityScope
@Component(modules = [ActivityModule::class],
           dependencies = [ApplicationComponent::class])
interface ActivityComponent {

    fun inject(activity: MainActivity)

}