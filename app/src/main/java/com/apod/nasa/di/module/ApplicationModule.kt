package com.apod.nasa.di.module

import android.content.Context
import com.apod.nasa.NasaApodApplication
import com.apod.nasa.data.local.ApodDao
import com.apod.nasa.data.local.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Pallab Banerjee on 3/8/2021.
 */

@Module
class ApplicationModule(private val application: NasaApodApplication) {

    @Provides
    @Singleton
    fun provideContext() : Context =  application


    @Provides
    @Singleton
    fun provideAppDatabase() : AppDatabase = AppDatabase.getInstance(application)


    @Provides
    fun provideApodDao(appDatabase: AppDatabase) : ApodDao = appDatabase.apodDao()

}