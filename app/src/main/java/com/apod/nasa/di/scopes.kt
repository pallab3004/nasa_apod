package com.apod.nasa.di

import javax.inject.Scope

/**
 * Created by Pallab Banerjee on 3/10/2021.
 */

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class ActivityScope