package com.apod.nasa.utils

/**
 * Created by Pallab Banerjee on 3/8/2021.
 */

/**
 * Constants used throughout the app.
 */
const val DATABASE_NAME = "nasa-apod-db"
const val DATA_FILENAME = "data.json"
const val IMAGE_CLICKED= "image_clicked"