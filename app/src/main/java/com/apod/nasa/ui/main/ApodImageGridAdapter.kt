package com.apod.nasa.ui.main

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.apod.nasa.data.model.ApodImageData
import com.apod.nasa.databinding.ItemApodImageBinding
import com.apod.nasa.ui.detail.DetailActivity
import com.apod.nasa.utils.IMAGE_CLICKED
import com.bumptech.glide.Glide

/**
 * Created by Pallab Banerjee on 3/10/2021.
 */

/**
 * Adapter for [RecyclerView] in [MainActivity]
 * */

class ApodImageGridAdapter : ListAdapter<ApodImageData, ApodImageGridAdapter.ApodImageViewHolder>(ApodImageDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ApodImageViewHolder {
       return ApodImageViewHolder(
           ItemApodImageBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
           )
       )
    }

    override fun onBindViewHolder(holder: ApodImageViewHolder, position: Int) {
        val imageData = getItem(position)
        holder.bind(imageData)
    }

    

    class ApodImageViewHolder(
        private val binding: ItemApodImageBinding
    ) : RecyclerView.ViewHolder(binding.root) {


        fun bind(imageData: ApodImageData) {

            Glide.with(binding.root.context).load(imageData.url).into(binding.ivApodImage)

            binding.apply {

                root.setOnClickListener { view ->

                    val intent = Intent(view.context, DetailActivity::class.java)
                    intent.apply {
                        putExtra(IMAGE_CLICKED, imageData.id)
                    }
                    view.context.startActivity(intent)
                }
            }

        }

    }



}

private class ApodImageDiffCallback : DiffUtil.ItemCallback<ApodImageData>() {
    override fun areItemsTheSame(oldItem: ApodImageData, newItem: ApodImageData): Boolean =
        oldItem.id == newItem.id


    override fun areContentsTheSame(oldItem: ApodImageData, newItem: ApodImageData): Boolean =
        oldItem == newItem

}