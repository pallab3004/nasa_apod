package com.apod.nasa.ui.main

import androidx.lifecycle.*
import com.apod.nasa.data.model.ApodImageData
import com.apod.nasa.data.repository.ApodImageListRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Pallab Banerjee on 3/9/2021.
 */


class ApodImageViewModel @Inject constructor(
    private val apodImageListRepository: ApodImageListRepository
) : ViewModel() {

   private val _apodImageList: MutableLiveData<List<ApodImageData>> = MutableLiveData()
    val apodImageList : LiveData<List<ApodImageData>> = _apodImageList

    fun getImageList(){
        viewModelScope.launch {
            apodImageListRepository.getApodImageList().collect{
                _apodImageList.postValue(it)
            }
        }
    }


}