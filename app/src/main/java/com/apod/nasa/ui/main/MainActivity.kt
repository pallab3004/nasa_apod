package com.apod.nasa.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import com.apod.nasa.NasaApodApplication
import com.apod.nasa.R
import com.apod.nasa.databinding.ActivityMainBinding
import com.apod.nasa.di.component.DaggerActivityComponent
import com.apod.nasa.di.module.ActivityModule
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var apodImageViewModel: ApodImageViewModel

    @Inject
    lateinit var bindingMain: ActivityMainBinding

    @Inject
    lateinit var apodImageGridAdapter: ApodImageGridAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
        setContentView(bindingMain.root)
        apodImageViewModel.getImageList()
        setUpUi()
    }

    private fun injectDependencies() {

        DaggerActivityComponent.builder()
            .applicationComponent((application as NasaApodApplication).applicationComponent)
            .activityModule(ActivityModule(this))
            .build()
            .inject(this)

    }


    override fun onStart() {
        super.onStart()

    }


    private fun setUpUi(){
    //    bindingMain = ActivityMainBinding.inflate(layoutInflater)

        apodImageViewModel.apodImageList.observe(this, Observer {
            apodImageGridAdapter.submitList(it)
        })
        bindingMain.rvApodImageGrid.adapter = apodImageGridAdapter

    }

    override fun onResume() {
        super.onResume()
    }

}