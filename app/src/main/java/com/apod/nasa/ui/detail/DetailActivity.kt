package com.apod.nasa.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.apod.nasa.R

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
    }
}