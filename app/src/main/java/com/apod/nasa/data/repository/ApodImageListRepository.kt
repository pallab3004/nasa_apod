package com.apod.nasa.data.repository

import com.apod.nasa.data.local.ApodDao
import com.apod.nasa.data.local.AppDatabase
import javax.inject.Inject

/**
 * Created by Pallab Banerjee on 3/9/2021.
 */

class ApodImageListRepository @Inject constructor(
   private val apodDao : ApodDao
) {

    fun getApodImageList()   = apodDao.getAllApodImageData()

}