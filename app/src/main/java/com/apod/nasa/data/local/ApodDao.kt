package com.apod.nasa.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.apod.nasa.data.model.ApodDetailsData
import com.apod.nasa.data.model.ApodImageData
import kotlinx.coroutines.flow.Flow

/**
 * Created by Pallab Banerjee on 3/8/2021.
 */

@Dao
interface ApodDao {

    @Query("SELECT id, date, url FROM apod ORDER BY id DESC")
    fun getAllApodImageData() : Flow<List<ApodImageData>>

    @Query("SELECT id, copyright, date, explanation, title, url FROM apod WHERE id = :apodId")
    fun getApodDetails(apodId : Long) : Flow<ApodDetailsData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(apodDataItem : List<ApodDataItem>)

}