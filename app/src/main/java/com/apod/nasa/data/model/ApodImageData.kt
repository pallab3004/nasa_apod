package com.apod.nasa.data.model

import androidx.room.ColumnInfo

/**
 * Created by Pallab Banerjee on 3/8/2021.
 */
/**
 * To be used to store instance of data required for the first screen (Image Grid screen)
* */

data class ApodImageData(
    val id : Long,
    val date: String,
    val url: String
)