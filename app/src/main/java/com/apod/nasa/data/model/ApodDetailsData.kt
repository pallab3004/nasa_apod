package com.apod.nasa.data.model

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

/**
 * Created by Pallab Banerjee on 3/8/2021.
 */
data class ApodDetailsData(
    val id: Long = 0,
    val copyright: String,
    val date: String,
    val explanation: String,
    val title: String,
    val url: String
)