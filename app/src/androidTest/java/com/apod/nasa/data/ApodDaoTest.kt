package com.apod.nasa.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.apod.nasa.data.local.ApodDao
import com.apod.nasa.data.local.ApodDataItem
import com.apod.nasa.data.local.AppDatabase
import com.apod.nasa.data.model.ApodDetailsData
import com.apod.nasa.data.model.ApodImageData
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.Matchers
import org.junit.*
import org.junit.runner.RunWith

/**
 * Created by Pallab Banerjee on 3/9/2021.
 */

@RunWith(AndroidJUnit4::class)
class ApodDaoTest {

    private lateinit var database: AppDatabase
    private lateinit var apodDao: ApodDao
    private val apodA = ApodDataItem(
        copyright = "ESA/HubbleNASA",
        date = "2019-12-01",
        explanation = "Why does this galaxy have a ring of bright blue stars?  Beautiful island universe Messier 94 lies a mere 15 million light-years distant in the northern constellation of the Hunting Dogs (Canes Venatici). A popular target for Earth-based astronomers, the face-on spiral galaxy is about 30,000 light-years across, with spiral arms sweeping through the outskirts of its broad disk. But this Hubble Space Telescope field of view spans about 7,000 light-years across M94's central region. The featured close-up highlights the galaxy's compact, bright nucleus, prominent inner dust lanes, and the remarkable bluish ring of young massive stars. The ring stars are all likely less than 10 million years old, indicating that M94 is a starburst galaxy that is experiencing an epoch of rapid star formation from inspiraling gas. The circular ripple of blue stars is likely a wave propagating outward, having been triggered by the gravity and rotation of a oval matter distributions. Because M94 is relatively nearby, astronomers can better explore details of its starburst ring.    Astrophysicists: Browse 2,000+ codes in the Astrophysics Source Code Library",
        hdurl = "https://apod.nasa.gov/apod/image/1912/M94_Hubble_1002.jpg",
        media_type = "image",
        service_version = "v1",
        title = "Starburst Galaxy M94 from Hubble",
        url = "https://apod.nasa.gov/apod/image/1912/M94_Hubble_960.jpg"
    )
    private val apodB = ApodDataItem(
        copyright = "Steve Mazlin",
        date = "2019-12-03",
        explanation = "Why does this galaxy have a ring of bright blue stars?  Beautiful island universe Messier 94 lies a mere 15 million light-years distant in the northern constellation of the Hunting Dogs (Canes Venatici). A popular target for Earth-based astronomers, the face-on spiral galaxy is about 30,000 light-years across, with spiral arms sweeping through the outskirts of its broad disk. But this Hubble Space Telescope field of view spans about 7,000 light-years across M94's central region. The featured close-up highlights the galaxy's compact, bright nucleus, prominent inner dust lanes, and the remarkable bluish ring of young massive stars. The ring stars are all likely less than 10 million years old, indicating that M94 is a starburst galaxy that is experiencing an epoch of rapid star formation from inspiraling gas. The circular ripple of blue stars is likely a wave propagating outward, having been triggered by the gravity and rotation of a oval matter distributions. Because M94 is relatively nearby, astronomers can better explore details of its starburst ring.    Astrophysicists: Browse 2,000+ codes in the Astrophysics Source Code Library",
        hdurl = "https://apod.nasa.gov/apod/image/1912/M94_Hubble_1002.jpg",
        media_type = "image",
        service_version = "v1",
        title = "Starburst Galaxy M94 from Hubble",
        url = "https://apod.nasa.gov/apod/image/1912/M94_Hubble_960.jpg"
    )

    private val apodImageA = ApodImageData(1, "2019-12-01","https://apod.nasa.gov/apod/image/1912/M94_Hubble_960.jpg")
    private val apodImageB = ApodImageData(2, "2019-12-03","https://apod.nasa.gov/apod/image/1912/M94_Hubble_960.jpg")


    private val apodDetailsData = ApodDetailsData(1, "ESA/HubbleNASA","2019-12-01","Why does this galaxy have a ring of bright blue stars?  Beautiful island universe Messier 94 lies a mere 15 million light-years distant in the northern constellation of the Hunting Dogs (Canes Venatici). A popular target for Earth-based astronomers, the face-on spiral galaxy is about 30,000 light-years across, with spiral arms sweeping through the outskirts of its broad disk. But this Hubble Space Telescope field of view spans about 7,000 light-years across M94's central region. The featured close-up highlights the galaxy's compact, bright nucleus, prominent inner dust lanes, and the remarkable bluish ring of young massive stars. The ring stars are all likely less than 10 million years old, indicating that M94 is a starburst galaxy that is experiencing an epoch of rapid star formation from inspiraling gas. The circular ripple of blue stars is likely a wave propagating outward, having been triggered by the gravity and rotation of a oval matter distributions. Because M94 is relatively nearby, astronomers can better explore details of its starburst ring.    Astrophysicists: Browse 2,000+ codes in the Astrophysics Source Code Library",
        "Starburst Galaxy M94 from Hubble","https://apod.nasa.gov/apod/image/1912/M94_Hubble_960.jpg")

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() = runBlocking {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        apodDao = database.apodDao()

        // Insert apod data
        apodDao.insertAll(listOf(apodA, apodB))
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun testGetAllApodImageData() = runBlocking {

        val apodImageList = apodDao.getAllApodImageData().first()
        assertThat(apodImageList.size, equalTo(2))

        Assert.assertThat(apodImageList[1], Matchers.equalTo(apodImageA))
        Assert.assertThat(apodImageList[0], Matchers.equalTo(apodImageB))

    }

    @Test
    fun testGetApodDetails() = runBlocking {

        val apodDetails = apodDao.getApodDetails(1).first()
        assertThat(apodDetails, equalTo(apodDetailsData))

    }


}