package com.apod.nasa.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import com.apod.nasa.data.model.ApodImageData
import com.apod.nasa.data.repository.ApodImageListRepository
import com.apod.nasa.ui.main.ApodImageViewModel
import com.apod.nasa.utils.TestCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Pallab Banerjee on 3/9/2021.
 */
@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ApodImageViewModelTest {

    @Mock
    lateinit var apodImageListRepository: ApodImageListRepository

    private lateinit var apodImageViewModel: ApodImageViewModel

    private val apodImageA = ApodImageData(1, "2019-12-01","https://apod.nasa.gov/apod/image/1912/M94_Hubble_960.jpg")
    private val apodImageB = ApodImageData(2, "2019-12-03","https://apod.nasa.gov/apod/image/1912/M94_Hubble_960.jpg")

    @get : Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()


    @get : Rule
    val testCoroutineRule = TestCoroutineRule()

    @Before
    fun setUp(){

        apodImageViewModel = ApodImageViewModel(apodImageListRepository)

    }

    @Test
    fun whenApodImageListLiveDataObserved_shouldGetDataFromApodImageRepository() = runBlocking{

        doReturn(flowOf(listOf(apodImageA, apodImageB)))
            .`when`(apodImageListRepository).getApodImageList()
        apodImageViewModel.getImageList()
        assertThat(apodImageViewModel.apodImageList.value, equalTo(listOf(apodImageA, apodImageB)))

    }


}