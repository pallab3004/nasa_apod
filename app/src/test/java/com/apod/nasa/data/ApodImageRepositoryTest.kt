package com.apod.nasa.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.apod.nasa.data.local.ApodDao
import com.apod.nasa.data.local.AppDatabase
import com.apod.nasa.data.model.ApodImageData
import com.apod.nasa.data.repository.ApodImageListRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Pallab Banerjee on 3/9/2021.
 */

@RunWith(MockitoJUnitRunner::class)
class ApodImageRepositoryTest {

    @Mock
    lateinit var apodDao: ApodDao

    private lateinit var apodImageRepository : ApodImageListRepository
    private val apodImageA = ApodImageData(1, "2019-12-01","https://apod.nasa.gov/apod/image/1912/M94_Hubble_960.jpg")
    private val apodImageB = ApodImageData(2, "2019-12-03","https://apod.nasa.gov/apod/image/1912/M94_Hubble_960.jpg")

   @Before
   fun setUp(){
       apodImageRepository = ApodImageListRepository(apodDao)
   }


    @Test
    fun whenCalledGetApodImageList_shouldCallGetAllApodImageDataFromApodDao(){

        doReturn(listOf(apodImageA,apodImageB).asFlow())
            .`when`(apodDao).getAllApodImageData()

        apodImageRepository.getApodImageList()

        verify(apodDao).getAllApodImageData()
    }

}